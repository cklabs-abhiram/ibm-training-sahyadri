# IBM Training Sahyadri

# Install Git
- Linux [Debian] - `sudo apt install git`
- Linux [Red Hat] - `sudo yum install git`
- [Windows](https://git-scm.com/download/win)
- [Mac](https://git-scm.com/download/mac)

# (Optional) Install Sublime Text
- [Download](https://sublimetext.com/3)

# Git Exercise 1

- Fork this repository
- Clone your fork
- Create a new file and name it \<your name\>.txt
- Write a brief introduction about yourself in that file
- Add the file to git
- Commit the file. In the commit message, write "Created introduction for \<your name\>"
- Push the file to origin master
- Open a Merge Request to my repository

# Git Exercise 2

- Fork this repository
- Clone your fork
- Open the file having the name of the person next to you
- Edit that file
- Add the file to git
- Commit the file, In the commit message, write "Added info to <filename>.txt"
- Push the file to origin master
- Open a Merge Request to my repository